package com.example.airfootballminigame.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.airfootballminigame.R
import com.example.airfootballminigame.ui.theme.Grey_transparent
import com.example.airfootballminigame.ui.theme.Grey_transparent_dark
import com.example.airfootballminigame.util.CurrentAppData

@Preview
@Composable
fun EndPopup(onBack : () -> Unit = {}, onRestart : () -> Unit = {}, winnerTop: Boolean = true) {
    val winnerColor = if(CurrentAppData.onePlayer && !winnerTop) Color.Green
    else if(CurrentAppData.onePlayer && winnerTop) Color.Red
    else if(!CurrentAppData.onePlayer && winnerTop) Color.Red
    else Color.Blue
    val header = if(CurrentAppData.onePlayer && !winnerTop) stringResource(id = R.string.header_win_default)
    else if(CurrentAppData.onePlayer && winnerTop) stringResource(id = R.string.header_lose_default)
    else if(!CurrentAppData.onePlayer && winnerTop) stringResource(id = R.string.header_win_top)
    else stringResource(id = R.string.header_win_bottom)
    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Grey_transparent), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .padding(Dp(10.0f))
                .background(color = Grey_transparent_dark, shape = RoundedCornerShape(Dp(20.0f)))
                .border(
                    width = Dp(3.0f),
                    color = winnerColor,
                    shape = RoundedCornerShape(Dp(20.0f))
                )
                .padding(Dp(10.0f)), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = header, fontSize = TextUnit(40.0f, TextUnitType.Sp), color = winnerColor, textAlign = TextAlign.Center)
            Row(modifier = Modifier.wrapContentSize(), horizontalArrangement = Arrangement.Center, verticalAlignment = Alignment.CenterVertically) {
                Button(onClick = onBack,
                    elevation = ButtonDefaults.elevation(defaultElevation = Dp(0.0f), pressedElevation = Dp(0.0f)),
                    contentPadding = PaddingValues(Dp(5.0f)),
                    shape = RoundedCornerShape(Dp(10.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent, contentColor = Color.White),
                    modifier = Modifier.weight(1.0f, true)) {
                    Image(modifier = Modifier.size(Dp(100.0f)), painter = painterResource(id = R.drawable.ic_baseline_arrow_back), contentDescription = stringResource(id = R.string.desc_back), contentScale = ContentScale.FillBounds)
                }
                Button(onClick = onRestart,
                    elevation = ButtonDefaults.elevation(defaultElevation = Dp(0.0f), pressedElevation = Dp(0.0f)),
                    contentPadding = PaddingValues(Dp(5.0f)),
                    shape = RoundedCornerShape(Dp(10.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent, contentColor = Color.White),
                    modifier = Modifier.weight(1.0f, true)) {
                    Image(modifier = Modifier.size(Dp(100.0f)), painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(id = R.string.desc_back), contentScale = ContentScale.FillBounds)
                }
            }
        }
    }
}