package com.example.airfootballminigame.ui.screens.gameScreen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.airfootballminigame.ui.screens.Screens
import com.example.airfootballminigame.util.Navigator
import com.example.airfootballminigame.view.AirFootballView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun GameScreen() {
    val viewModel = viewModel<GameViewModel>()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        AndroidView(factory = { AirFootballView(it) },
            modifier = Modifier.fillMaxSize(),
            update = { view ->
                viewModel.restartFlow.onEach { if(it) view.restart() }.launchIn(viewModel.viewModelScope)
                view.setInterface(object: AirFootballView.AirFootballInterface {
                    override fun onEnd(winnerTop: Boolean) {
                        viewModel.end(winnerTop)
                    }
                })
            })
    }

    val endPopup by viewModel.endPopup.collectAsState(false)

    if(endPopup) EndPopup({ Navigator.navigateTo(Screens.GAME_MENU) }, viewModel::restart, viewModel.topIsWinner)
}