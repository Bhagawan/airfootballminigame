package com.example.airfootballminigame.ui.screens

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.airfootballminigame.R
import com.example.airfootballminigame.ui.theme.Green
import com.example.airfootballminigame.ui.theme.Grey
import com.example.airfootballminigame.ui.theme.Orange
import com.example.airfootballminigame.ui.theme.Red
import com.example.airfootballminigame.util.CurrentAppData
import com.example.airfootballminigame.util.Navigator

@Composable
fun GameMenu() {
    val back = remember { CurrentAppData.getAsset("back") }
    val activity = (LocalContext.current as? Activity)
    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Green)) {
        back?.bitmap?.asImageBitmap()?.let { Image(it, contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = if(it.width > it.height) ContentScale.FillHeight else ContentScale.FillBounds) }
        Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                TextButton(onClick = { Navigator.navigateTo(Screens.GAME_SCREEN) },
                    elevation = ButtonDefaults.elevation(Dp(10.0f)),
                    modifier = Modifier.wrapContentSize().border(width = Dp(2.0f), color = Grey, shape = RoundedCornerShape(Dp(20.0f))),
                    shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Orange, contentColor = Color.White)){
                    Text(text = stringResource(id = R.string.btn_one_player), fontSize = TextUnit(30.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White)
                }
            }
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                TextButton(
                    onClick = {
                        CurrentAppData.onePlayer = false
                        Navigator.navigateTo(Screens.GAME_SCREEN)
                    },
                    elevation = ButtonDefaults.elevation(Dp(10.0f)),
                    modifier = Modifier.wrapContentSize().border(width = Dp(2.0f), color = Grey, shape = RoundedCornerShape(Dp(20.0f))),
                    shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Orange, contentColor = Color.White)){
                    Text(text = stringResource(id = R.string.btn_two_players), fontSize = TextUnit(30.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White)
                }
            }
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                TextButton(onClick = { activity?.finish() },
                    elevation = ButtonDefaults.elevation(Dp(10.0f)),
                    modifier = Modifier.wrapContentSize().border(width = Dp(2.0f), color = Grey, shape = RoundedCornerShape(Dp(20.0f))),
                    shape = RoundedCornerShape(Dp(20.0f)),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Red, contentColor = Color.White)){
                    Text(text = stringResource(id = R.string.btn_exit), fontSize = TextUnit(20.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White)
                }
            }
        }
    }
}