package com.example.airfootballminigame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    ASSETS_LOADING_ERROR_SCREEN("assets_loading_error"),
    WEB_VIEW("web_view"),
    GAME_MENU("game_menu"),
    GAME_SCREEN("game")
}