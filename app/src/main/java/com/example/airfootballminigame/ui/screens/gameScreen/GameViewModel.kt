package com.example.airfootballminigame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameViewModel: ViewModel() {
    private val _endPopup = MutableStateFlow(false)
    val endPopup = _endPopup.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    var topIsWinner = true

    fun restart() {
        _endPopup.tryEmit(false)
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }

    fun end(topWinner: Boolean) {
        topIsWinner = topWinner
        _endPopup.tryEmit(true)
    }
}