package com.example.airfootballminigame.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import com.example.airfootballminigame.util.Coordinate
import com.example.airfootballminigame.util.CurrentAppData
import com.example.airfootballminigame.util.FCoordinate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class AirFootballView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var ballSize = 1.0f
    private var playerSize = 1.0f

    private var borderWidth = 0.0f
    private var goalOff = 1.0f

    private val topPlayer = Coordinate(0,0)
    private val bottomPlayer = Coordinate(0,0)
    private val ballCoord = FCoordinate(0.0f,0.0f)

    private val ballSpeed = FCoordinate(0.0f,0.0f)
    private val onePlayer = CurrentAppData.onePlayer

    companion object {
        const val STATE_GAME = 0
        const val STATE_PAUSE = 1
    }
    private val ball = CurrentAppData.getAsset("ball")
    private var field :Bitmap? = null

    private var topScore = 0
    private var bottomScore = 0

    private var state = STATE_GAME

    private var mInterface: AirFootballInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            playerSize = if(mWidth < mHeight) mHeight * 0.1f else mWidth * 0.1f
            ballSize = if(mWidth < mHeight) mHeight * 0.05f else mWidth * 0.05f
            goalOff = if(mWidth > mHeight) mHeight * 0.4f else mWidth * 0.3f
            borderWidth = if(mWidth > mHeight) mHeight / 20.0f else mWidth / 20.0f
            updateFieldBitmap()
            reset()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
            if(state == STATE_GAME) {
                updateBall()
                if(onePlayer) updateComputerPlayer()
            }
            drawScore(it)
            drawBall(it)
            drawPlayers(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_POINTER_DOWN,
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_POINTER_UP -> {
                if(state == STATE_GAME) checkTouch(event.x, event.y)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if(state == STATE_GAME) {
                    for(n in 0 until event.pointerCount) {
                        val id = event.getPointerId(n)
                        checkTouch(event.getX(id), event.getY(id))
                    }
                    checkTouch(event.x, event.y)
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: AirFootballInterface) {
        mInterface = i
    }

    fun restart() {
        topScore = 0
        bottomScore = 0
        reset()
        state = STATE_GAME
    }

    //// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        if(field != null) {
            c.drawBitmap(field!!, null, Rect(0,0, mWidth, mHeight), p)
        } else {
            c.drawColor(Color.parseColor("#5C90F16E"))
        }
        if(mWidth > mHeight) {
            p.color = Color.parseColor("#3C3C3C")
            p.style = Paint.Style.FILL
            c.drawRect(0.0f,0.0f, mWidth.toFloat(), borderWidth, p)
            c.drawRect(0.0f,mHeight - borderWidth, mWidth.toFloat(), mHeight.toFloat(), p)
            c.drawRect(0.0f,0.0f, borderWidth, goalOff, p)
            c.drawRect(0.0f,mHeight - goalOff, borderWidth, mHeight.toFloat(), p)
            p.color = Color.WHITE
            c.drawRect(0.0f,goalOff, borderWidth, mHeight - goalOff, p)
            p.color = Color.parseColor("#3C3C3C")
            c.drawRect(mWidth - borderWidth,0.0f, mWidth.toFloat(), goalOff, p)
            c.drawRect(mWidth - borderWidth,mHeight - goalOff, mWidth.toFloat(), mHeight.toFloat(), p)
            p.color = Color.WHITE
            c.drawRect(mWidth - borderWidth,goalOff, mWidth.toFloat(), mHeight - goalOff, p)
        } else {
            p.color = Color.parseColor("#3C3C3C")
            p.style = Paint.Style.FILL
            c.drawRect(0.0f,0.0f, borderWidth, mHeight.toFloat(), p)
            c.drawRect(mWidth - borderWidth,0.0f, mWidth.toFloat(), mHeight.toFloat(), p)
            c.drawRect(0.0f,0.0f, goalOff, borderWidth, p)
            c.drawRect(mWidth - goalOff,0.0f, mWidth.toFloat(), borderWidth, p)
            p.color = Color.WHITE
            c.drawRect(goalOff, 0.0f, mWidth - goalOff, borderWidth, p)
            p.color = Color.parseColor("#3C3C3C")
            c.drawRect(0.0f,mHeight - borderWidth, goalOff, mHeight.toFloat(), p)
            c.drawRect(mWidth - goalOff,mHeight - borderWidth, mWidth.toFloat(), mHeight.toFloat(), p)
            p.color = Color.WHITE
            c.drawRect(goalOff, mHeight - borderWidth, mWidth - goalOff, mHeight.toFloat(), p)
        }
    }

    private fun drawPlayers(c: Canvas) {
        val p = Paint()
        p.color = Color.RED
        p.style = Paint.Style.FILL
        c.drawCircle(topPlayer.x.toFloat(), topPlayer.y.toFloat(), playerSize / 2, p)
        p.color = Color.parseColor("#591515")
        p.style = Paint.Style.STROKE
        c.drawCircle(topPlayer.x.toFloat(), topPlayer.y.toFloat(), playerSize / 2, p)
        p.color = Color.BLUE
        p.style = Paint.Style.FILL
        c.drawCircle(bottomPlayer.x.toFloat(), bottomPlayer.y.toFloat(), playerSize / 2, p)
        p.color = Color.parseColor("#0E4168")
        p.style = Paint.Style.STROKE
        c.drawCircle(bottomPlayer.x.toFloat(), bottomPlayer.y.toFloat(), playerSize / 2, p)
    }

    private fun drawBall(c: Canvas) {
        ball?.let {
            val p = Paint()
            c.drawBitmap(it.bitmap, null, Rect((ballCoord.x - ballSize / 2).toInt(), (ballCoord.y - ballSize / 2).toInt(), (ballCoord.x + ballSize / 2).toInt(), (ballCoord.y + ballSize / 2).toInt()), p)
        }
    }

    private fun updateBall() {
        val distanceToTop = sqrt((topPlayer.x - (ballCoord.x + ballSpeed.x)).pow(2) + (topPlayer.y - (ballCoord.y + ballSpeed.y)).pow(2))
        val distanceToBottom = sqrt((bottomPlayer.x - (ballCoord.x + ballSpeed.x)).pow(2) + (bottomPlayer.y - (ballCoord.y + ballSpeed.y)).pow(2))
        if(distanceToTop <= (ballSize + playerSize) / 2) {
            var angle = 180 - 2 * Math.toDegrees(acos(((topPlayer.x - ballCoord.x) * ballSpeed.x + (topPlayer.y - ballCoord.y) * ballSpeed.y) / (sqrt((topPlayer.x - ballCoord.x).pow(2) + (topPlayer.y - ballCoord.y).pow(2)) * sqrt(ballSpeed.x.pow(2) + ballSpeed.y.pow(2)))).toDouble()).absoluteValue.toInt().coerceAtMost(90)
            if(ballCoord.y >= topPlayer.y ) {
                if(ballCoord.x <= topPlayer.x) {
                    if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0)) angle *= -1
                } else if(!(ballSpeed.x <= 0 && ballSpeed.y >= 0)) angle *= -1
            } else {
                if(ballCoord.x <= topPlayer.x) {
                    if((ballSpeed.x >= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue) || (ballSpeed.x <=0 && ballSpeed.y <= 0)) angle *= -1
                } else if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0) || (ballSpeed.x <= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue)) angle *= -1
            }
            ballCoord.x += ballSpeed.x
            ballCoord.y += ballSpeed.y
            val newX = ballSpeed.x * cos(Math.toRadians(angle.toDouble())) - ballSpeed.y * sin(Math.toRadians(angle.toDouble()))
            val newY = ballSpeed.x * sin(Math.toRadians(angle.toDouble())) + ballSpeed.y * cos(Math.toRadians(angle.toDouble()))
            ballSpeed.x = newX.toFloat()
            ballSpeed.y = newY.toFloat()
            ballCoord.x += ballSpeed.x
            ballCoord.y += ballSpeed.y
        } else if(distanceToBottom <= (ballSize + playerSize) / 2) {
            var angle = 180 - 2 * Math.toDegrees(acos(((bottomPlayer.x - ballCoord.x) * ballSpeed.x + (bottomPlayer.y - ballCoord.y) * ballSpeed.y) / (sqrt((bottomPlayer.x - ballCoord.x).pow(2) + (bottomPlayer.y - ballCoord.y).pow(2)) * sqrt(ballSpeed.x.pow(2) + ballSpeed.y.pow(2)))).toDouble()).absoluteValue.toInt().coerceAtMost(90)
            if(ballCoord.y >= bottomPlayer.y ) {
                if(ballCoord.x <= bottomPlayer.x) {
                    if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0)) angle *= -1
                } else if(!(ballSpeed.x <= 0 && ballSpeed.y >= 0)) angle *= -1
            } else {
                if(ballCoord.x <= bottomPlayer.x) {
                    if((ballSpeed.x >= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue) || (ballSpeed.x <=0 && ballSpeed.y <= 0)) angle *= -1
                } else if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0) || (ballSpeed.x <= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue)) angle *= -1
            }
            ballCoord.x += ballSpeed.x
            ballCoord.y += ballSpeed.y
            val newX = ballSpeed.x * cos(Math.toRadians(angle.toDouble())) - ballSpeed.y * sin(Math.toRadians(angle.toDouble()))
            val newY = ballSpeed.x * sin(Math.toRadians(angle.toDouble())) + ballSpeed.y * cos(Math.toRadians(angle.toDouble()))
            ballSpeed.x = newX.toFloat()
            ballSpeed.y = newY.toFloat()
            ballCoord.x += ballSpeed.x
            ballCoord.y += ballSpeed.y
        } else {
            ballCoord.x += ballSpeed.x
            ballCoord.y += ballSpeed.y
        }
        if(ballCoord.x <= ballSize / 2 + borderWidth) {
            if(mWidth > mHeight && (ballCoord.y in (goalOff + ballSize / 2.0f)..(mHeight - goalOff - ballSize / 2.0f))) addPoint(false)
            else {
                ballCoord.x = ballSize / 2 + borderWidth
                ballSpeed.x = ballSpeed.x.absoluteValue
            }
        }
        if(ballCoord.x >= mWidth - ballSize / 2 - borderWidth) {
            if(mWidth > mHeight && (ballCoord.y in (goalOff + ballSize / 2.0f)..(mHeight - goalOff - ballSize / 2.0f))) addPoint(true)
            else {
                ballCoord.x = mWidth - ballSize / 2.0f - borderWidth
                ballSpeed.x = ballSpeed.x.absoluteValue * -1
            }
        }
        if(ballCoord.y <= ballSize / 2 + borderWidth) {
            if(mWidth < mHeight && (ballCoord.x in (goalOff + ballSize / 2.0f)..(mWidth - goalOff - ballSize / 2.0f))) addPoint(false)
            else {
                ballCoord.y = ballSize / 2.0f + borderWidth
                ballSpeed.y = ballSpeed.y.absoluteValue
            }
        }
        if(ballCoord.y >= mHeight - ballSize / 2 - borderWidth) {
            if(mWidth < mHeight && (ballCoord.x in (goalOff + ballSize / 2.0f)..(mWidth - goalOff - ballSize / 2.0f))) addPoint(true)
            else {
                ballCoord.y = mHeight - ballSize / 2.0f - borderWidth
                ballSpeed.y = ballSpeed.y.absoluteValue * -1
            }
        }
    }

    private fun movePlayer(top: Boolean, newX: Float, newY: Float) {
        val nX: Float
        val nY: Float
        if(top) {
            nX = newX.coerceAtLeast(playerSize / 2 + borderWidth).coerceAtMost(if(mWidth < mHeight) (mWidth - playerSize / 2 - borderWidth) else ((mWidth - playerSize) / 2))
            nY = newY.coerceAtLeast(playerSize / 2 + borderWidth).coerceAtMost(if(mWidth < mHeight) ((mHeight - playerSize) / 2) else (mHeight - playerSize / 2 - borderWidth))
            val distanceToTop = sqrt((nX - ballCoord.x).pow(2) + (nY - ballCoord.y).pow(2))
            if(distanceToTop <= (ballSize + playerSize) / 2) {
                var angle = 180 - 2 * Math.toDegrees(acos(((nX - ballCoord.x) * ballSpeed.x + (nY - ballCoord.y) * ballSpeed.y) / (sqrt((nX - ballCoord.x).pow(2) + (nY - ballCoord.y).pow(2)) * sqrt(
                    ballSpeed.x.pow(2) + ballSpeed.y.pow(2)))).toDouble()).absoluteValue.toInt().coerceAtMost(90)
                if(ballCoord.y >= nY ) {
                    if(ballCoord.x <= nX) {
                        if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0)) angle *= -1
                    } else if(!(ballSpeed.x <= 0 && ballSpeed.y >= 0)) angle *= -1
                } else {
                    if(ballCoord.x <= nX) {
                        if((ballSpeed.x >= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue) || (ballSpeed.x <=0 && ballSpeed.y <= 0)) angle *= -1
                    } else if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0) || (ballSpeed.x <= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue)) angle *= -1
                }
                val impulsX = (nX - topPlayer.x) / 100
                val impulsY = (nY - topPlayer.y )/ 100
                val newXX = ballSpeed.x * cos(Math.toRadians(angle.toDouble())) - ballSpeed.y * sin(Math.toRadians(angle.toDouble())) + impulsX
                val newYY = ballSpeed.x * sin(Math.toRadians(angle.toDouble())) + ballSpeed.y * cos(Math.toRadians(angle.toDouble())) + impulsY
                ballSpeed.x = newXX.toFloat() + (newX - topPlayer.x)
                ballSpeed.y = newYY.toFloat() + (newY - topPlayer.y)
                ballCoord.x += ballSpeed.x
                ballCoord.y += ballSpeed.y
            }
        } else {
            nX = newX.coerceAtLeast(if(mWidth < mHeight) playerSize / 2 + borderWidth else ((mWidth + ballSize) / 2)).coerceAtMost(mWidth - playerSize / 2 - borderWidth)
            nY = newY.coerceAtLeast(if(mWidth < mHeight) ( mHeight + playerSize) / 2 else ballSize / 2 + borderWidth).coerceAtMost(mHeight - playerSize / 2 - borderWidth)
            val distanceToBottom = sqrt((nX - ballCoord.x).pow(2) + (nY - ballCoord.y).pow(2))
            if(distanceToBottom <= (ballSize + playerSize) / 2) {
                var angle = 180 - 2 * Math.toDegrees(acos(((nX - ballCoord.x) * ballSpeed.x + (nY - ballCoord.y) * ballSpeed.y) / (sqrt(
                    (nX - ballCoord.x).pow(2) + (nY - ballCoord.y).pow(2)) * sqrt(ballSpeed.x.pow(2) + ballSpeed.y.pow(2)))).toDouble()).absoluteValue.toInt().coerceAtMost(90)
                if(ballCoord.y >= nY ) {
                    if(ballCoord.x <= nX) {
                        if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0)) angle *= -1
                    } else if(!(ballSpeed.x <= 0 && ballSpeed.y >= 0)) angle *= -1
                } else {
                    if(ballCoord.x <= nX) {
                        if((ballSpeed.x >= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue) || (ballSpeed.x <=0 && ballSpeed.y <= 0)) angle *= -1
                    } else if(!(ballSpeed.x <= 0 && ballSpeed.y <= 0) || (ballSpeed.x <= 0 && ballSpeed.y.absoluteValue < ballSpeed.x.absoluteValue)) angle *= -1
                }
                ballCoord.x += ballSpeed.x
                ballCoord.y += ballSpeed.y
                val newXX = ballSpeed.x * cos(Math.toRadians(angle.toDouble())) - ballSpeed.y * sin(Math.toRadians(angle.toDouble()))
                val newYY = ballSpeed.x * sin(Math.toRadians(angle.toDouble())) + ballSpeed.y * cos(Math.toRadians(angle.toDouble()))
                ballSpeed.x = newXX.toFloat() + (newX - bottomPlayer.x)
                ballSpeed.y = newYY.toFloat() + (newY - bottomPlayer.y)
                ballCoord.x += ballSpeed.x
                ballCoord.y += ballSpeed.y
            }
        }
        if(ballCoord.x <= ballSize / 2 + borderWidth) {
            if(mWidth > mHeight && (ballCoord.y in (goalOff + ballSize / 2.0f)..(mHeight - goalOff - ballSize / 2.0f))) addPoint(false)
            else {
                ballCoord.x = ballSize / 2 + borderWidth
                ballSpeed.x = ballSpeed.x.absoluteValue
            }
        }
        if(ballCoord.x >= mWidth - ballSize / 2 - borderWidth) {
            if(mWidth > mHeight && (ballCoord.y in (goalOff + ballSize / 2.0f)..(mHeight - goalOff - ballSize / 2.0f))) addPoint(true)
            else {
                ballCoord.x = mWidth - ballSize / 2.0f - borderWidth
                ballSpeed.x = ballSpeed.x.absoluteValue * -1
            }
        }
        if(ballCoord.y <= ballSize / 2 + borderWidth) {
            if(mWidth < mHeight && (ballCoord.x in (goalOff + ballSize / 2.0f)..(mWidth - goalOff - ballSize / 2.0f))) addPoint(false)
            else {
                ballCoord.y = ballSize / 2.0f + borderWidth
                ballSpeed.y = ballSpeed.y.absoluteValue
            }
        }
        if(ballCoord.y >= mHeight - ballSize / 2 - borderWidth) {
            if(mWidth < mHeight && (ballCoord.x in (goalOff + ballSize / 2.0f)..(mWidth - goalOff - ballSize / 2.0f))) addPoint(true)
            else {
                ballCoord.y = mHeight - ballSize / 2.0f - borderWidth
                ballSpeed.y = ballSpeed.y.absoluteValue * -1
            }
        }
        val dist = sqrt((newX - ballCoord.x).pow(2) + (newY - ballCoord.y).pow(2))
        if(dist >= (ballSize + playerSize) / 2 ) {
            if(top) {
                topPlayer.x = nX.toInt()
                topPlayer.y = nY.toInt()
            } else {
                bottomPlayer.x = nX.toInt()
                bottomPlayer.y = nY.toInt()
            }
        }
    }

    private fun updateComputerPlayer() {
        if(mWidth < mHeight) {
            if(ballSpeed.y < 0 && (ballCoord.y < mHeight / 3)) stepComputerPlayerTo(ballCoord.x.toInt(), ballCoord.y.toInt())
            else stepComputerPlayerTo(mWidth / 2, (playerSize / 2 + borderWidth).toInt())
        } else {
            if(ballSpeed.x < 0 && (ballCoord.x < mWidth / 3)) stepComputerPlayerTo(ballCoord.x.toInt(), ballCoord.y.toInt())
            else stepComputerPlayerTo((playerSize / 2 + borderWidth).toInt(), mHeight / 2)
        }
    }

    private fun stepComputerPlayerTo(x: Int, y: Int) {
        val speedX = mWidth / 400.0f
        val speedY = mHeight / 400.0f
        val dX = if((x - topPlayer.x ).absoluteValue < speedX) x - topPlayer.x else (x - topPlayer.x).sign * speedX
        val dY = if((y - topPlayer.y).absoluteValue < speedY) y - topPlayer.y else (y - topPlayer.y).sign * speedY
        topPlayer.x += dX.toInt()
        topPlayer.y += dY.toInt()
    }

    private fun drawScore(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.isFakeBoldText = true
        p.textSize = 100.0f
        p.color = Color.WHITE
        if(mWidth < mHeight) {
            c.save()
            c.rotate(90.0f, mWidth / 2.0f - 30, mHeight / 2.0f)
            c.drawText("$topScore : $bottomScore", mWidth / 2.0f - 30, mHeight / 2.0f, p)
            c.restore()
        } else c.drawText("$topScore : $bottomScore", mWidth / 2.0f, mHeight / 2.0f - 30, p)
    }

    private fun addPoint(top: Boolean) {
        if(top) {
            topScore++
            if(topScore >= 5) {
                mInterface?.onEnd(true)
                state = STATE_PAUSE
            } else reset()
        } else {
            bottomScore++
            if(bottomScore >= 5) {
                mInterface?.onEnd(false)
                state = STATE_PAUSE
            } else reset()
        }
    }

    private fun reset() {
        topPlayer.x = if(mWidth > mHeight) mWidth / 4 else mWidth / 2
        topPlayer.y = if(mWidth > mHeight) mWidth / 2 else mWidth / 4
        bottomPlayer.x = if(mWidth > mHeight) (mWidth / 4) * 3 else mWidth / 2
        bottomPlayer.y = if(mWidth > mHeight) mHeight / 2 else (mHeight / 4) * 3
        ballCoord.x = mWidth / 2.0f
        ballCoord.y = mHeight / 2.0f
        ballSpeed.x = Random.nextFloat()
        ballSpeed.y = Random.nextFloat()
    }

    private fun checkTouch(x: Float, y: Float) {
        var top = true
        if((mWidth > mHeight && x > mWidth / 2) || (mWidth < mHeight && y > mHeight / 2))  top = false
        if (onePlayer) top = false
        movePlayer(top, x, y)
    }

    private fun updateFieldBitmap() {
        CurrentAppData.getAsset("back")?.let {
            field = if(mWidth < mHeight) {
                val m = Matrix()
                m.postRotate(90.0f)
                Bitmap.createBitmap(it.bitmap, 0,0, it.bitmap.width, it.bitmap.height, m, true)
            } else it.bitmap
        }

    }

    interface AirFootballInterface {
        fun onEnd(winnerTop: Boolean)
    }
}