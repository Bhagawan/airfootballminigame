package com.example.airfootballminigame.util

data class Coordinate(var x: Int, var y: Int)