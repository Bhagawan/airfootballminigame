package com.example.airfootballminigame.util

import androidx.annotation.Keep

@Keep
data class AirFootballSplashResponse(val url : String)