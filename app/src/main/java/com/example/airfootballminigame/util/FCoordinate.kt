package com.example.airfootballminigame.util

data class FCoordinate(var x: Float, var y: Float)
