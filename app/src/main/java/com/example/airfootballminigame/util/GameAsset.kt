package com.example.airfootballminigame.util

import android.graphics.Bitmap

data class GameAsset(val name: String, val bitmap: Bitmap) {
    override fun equals(other: Any?): Boolean {
        return if(other is GameAsset) other.name == name
        else false
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + bitmap.hashCode()
        return result
    }
}
