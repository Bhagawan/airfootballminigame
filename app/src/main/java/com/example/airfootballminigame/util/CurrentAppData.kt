package com.example.airfootballminigame.util

object CurrentAppData {
    var url = ""
    var onePlayer = true

    val gameAssets = ArrayList<GameAsset>()

    fun getAsset(name: String): GameAsset? {
        for(asset in gameAssets) if(asset.name == name) return asset
        return null
    }
}